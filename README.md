# Task with Lumen PHP Framework

### System requirements (just requirements for lumen php framework v8):
* php 7.4 or greater
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension


### Steps to install and run:
* Create `.env` file from `.env.example` file
* Run: ``` composer install ```
* For testing task from command line (`inputs1.csv` means filename which transactions should be calculated):
  * `` php artisan comission:calculate inputs1.csv ``
* If you want to calculate another csv files then put them to `public/csv` folder and run:
  * `` php artisan comission:calculate yourfilename ``
* Note: In the future you can create another fileparser (example: XML parser) and also use it. For this purpose you can do:
  * Create your file parser class inside `/app/Services/ComissionCalculator/FileParser` and extend it from `BaseParser`
  * Add it to configuration inside `/config/app.php` file
* All tests are inside `/tests/` folder and they can be run with phpunit


### Additional information about configuration and classes
* If you want to add new calculator class:
  * Create your class inside `/app/Services/ComissionCalculator/Calculator` and extend it from `BaseCalculator` class
  * Open `/config/app.php` and add it to `calculators` array. At the moment it is like this:
    ```
    'calculators' => [
        'deposit' => [
            'private' => DepositCalculator::class,
            'business' => DepositCalculator::class
        ],
        'withdraw' => [
            'private' => PrivateWithdrawCalculator::class,
            'business' => BusinessWithdrawCalculator::class
        ]
    ],
    ```
* All commission numbers related configurations are inside `.env` file. You can change them from there
* All currencies are inside `/config/app.php` file
* Decimal numbers for amounts (for rounding up) are set in `/config/app.php` file. But you can also enable it to get
automatically by setting `USE_DECIMALS_CONFIG` to `false` in `.env` file
