<?php
namespace App\Console\Commands;

use App\Services\ComissionCalculator\ComissionCalculatorService;
use Exception;
use Illuminate\Console\Command;

class CalculateComissionCommand extends Command
{

    /**
     * Command signature. Example: php artisan comission:calculate input1.csv
     * @var string
     */
    protected $signature = "comission:calculate {filename}";

    /**
     * Command description
     * @var string
     */
    protected $description = "Calculate comission by filename";

    /**
     * File parsers
     * @var array
     */
    protected array $fileParsers = [];

    public function __construct()
    {
        parent::__construct();
        $this->fileParsers = config('app.fileParsers');
    }

    /**
     * Make command
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        $filename = $this->argument('filename');
        $array = explode('.', $filename);
        $ext = end($array);

        try {
            $parser = app()->make($this->fileParsers[$ext]);
            $parser->parse($filename);
            $parser->transformData();
            $data = $parser->getTransformedData();
            $cc = new ComissionCalculatorService();
            $tableHeaders = ['Date', 'User Id', 'User Type', 'Operation Type', 'Amount', 'Currency', 'Commission'];
            $rows = [];
            $comissions = [];

            foreach ($data as $d) {
                $date = $d->getDate();
                $userId = $d->getUserId();
                $userType = $d->getUserType();
                $operationType = $d->getOperationType();
                $amount = $d->getAmount();
                $currency = $d->getCurrency();
                $comission = $cc->calculateComission($d);
                $comission = is_int($comission) ? number_format($comission, 2): $comission;

                $rows[] = [$date, $userId, $userType, $operationType, $amount, $currency, $comission];
                $comissions[] = $comission;
            }
            $this->alert('Calculating comissions from file '.$filename.' - show all data');
            sleep(2);
            $this->table($tableHeaders, $rows);

            $this->alert('Show only commissions');
            foreach ($comissions as $comission) {
                $this->info($comission);
            }
        } catch (\Exception $ex) {
            $this->error($ex->getMessage());
        }

    }
}
