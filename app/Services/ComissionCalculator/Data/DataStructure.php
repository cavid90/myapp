<?php

namespace App\Services\ComissionCalculator\Data;

class DataStructure implements IDataStructure
{
    /**
     * Array of transactions
     * @var array
     */
    protected array $data;

    /**
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get date from array
     * @return string
     */
    public function getDate():string
    {
        return $this->data[0];
    }

    /**
     * Get user id from array
     * @return int
     */
    public function getUserId():int
    {
        return $this->data[1];
    }

    /**
     * Get user type from array
     * @return string
     */
    public function getUserType():string
    {
        return $this->data[2];
    }

    /**
     * Get operation type from array
     * @return string
     */
    public function getOperationType():string
    {
        return $this->data[3];
    }

    /**
     * Get amount from array
     * @return float|int
     */
    public function getAmount()
    {
        return $this->data[4];
    }

    /**
     * Get currency from array
     * @return string
     */
    public function getCurrency():string
    {
        return $this->data[5];
    }
}
