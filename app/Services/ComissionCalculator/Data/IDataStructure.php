<?php

namespace App\Services\ComissionCalculator\Data;

interface IDataStructure
{
    public function getDate():string;
    public function getUserId():int;
    public function getUserType():string;
    public function getOperationType():string;
    public function getAmount();
    public function getCurrency():string;
}
