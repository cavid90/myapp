<?php

namespace App\Services\ComissionCalculator;


use Laravel\Lumen\Application;

class Currency
{
    /**
     * Currency rates
     * @var array|Application|mixed
     */
    protected $rates = [];

    /**
     * Base currency, default is EUR
     * @var Application|mixed
     */
    protected $baseCurrency;

    public function __construct()
    {
        $this->baseCurrency = config('app.currencies.base');
        $this->rates = config('app.currencies.rates');
    }

    /**
     * Return base currency
     * @return Application|mixed
     */
    public function getBaseCurrency()
    {
        return $this->baseCurrency;
    }

    /**
     * Return currency rates
     * @return array|Application|mixed
     */
    public function getRates()
    {
        return $this->rates;
    }

    /**
     * Convert rate to base currency
     * @param $amount
     * @param $currency
     * @return float|int
     */
    public function convertToBaseCurrency($amount, $currency) {
        return $amount / $this->getRate($currency);
    }

    /**
     * Convert from base rate to selected rate
     * @param $amount
     * @param $currency
     * @return float|int
     */
    public function convertFromBaseCurrency($amount, $currency)
    {
        return $amount * $this->getRate($currency);
    }

    /**
     * Get rate of a currency
     * @param $currency
     * @return mixed|object
     */
    public function getRate($currency)
    {
        return $this->rates[$currency];
    }
}
