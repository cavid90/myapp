<?php
namespace App\Services\ComissionCalculator;

use App\Services\ComissionCalculator\Data\IDataStructure;
use Exception;

class ComissionCalculatorService
{
    /**
     * Array of calculators
     * @var array
     */
    private array $calculators = [];

    /**
     * Construct and set calculator array
     */
    public function __construct()
    {
        foreach (config('app.calculators') as $operationName => $userTypes) {
            foreach ($userTypes as $userTypeName => $operationClass) {
                $this->calculators[$userTypeName.'_'.$operationName] = app()->make($operationClass);
            }
        }
    }

    /**
     * @param IDataStructure $data
     * @return float|int|Exception
     * @throws Exception
     */
    public function calculateComission(IDataStructure $data)
    {
        $calculator = $this->selectCalculator($data->getOperationType(), $data->getUserType());
        return $calculator->calculate($data);
    }

    /**
     * @param $operationType
     * @param $userType
     * @return mixed
     * @throws Exception
     */
    protected function selectCalculator($operationType, $userType) {
        $calculatorKey = $userType.'_'.$operationType;

        if(array_key_exists($calculatorKey, $this->calculators)) {
            return $this->calculators[$userType.'_'.$operationType];
        }
        throw new Exception('Calculator not found');
    }
}
