<?php

namespace App\Services\ComissionCalculator\Calculator\Deposit;

use App\Services\ComissionCalculator\Calculator\BaseCalculator;
use App\Services\ComissionCalculator\Data\IDataStructure;

class DepositCalculator extends BaseCalculator
{
    /**
     * Get fee rate in percent
     * @return float|int
     */
    public function getFee(): float {
        return env('DEPOSIT_COMISSION_FEE') / 100;
    }

    /**
     * Calculate commission
     * @param IDataStructure $transaction
     * @return float|int
     */
    public function calculate(IDataStructure $transaction)
    {
        $decimals = $this->getNumberOfDecimals($transaction->getAmount(), $transaction->getCurrency());
        return $this->roundUpComissionFee($transaction->getAmount() * $this->getFee(), $decimals);
    }
}
