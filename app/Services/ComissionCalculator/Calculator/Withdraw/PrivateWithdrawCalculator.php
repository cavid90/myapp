<?php

namespace App\Services\ComissionCalculator\Calculator\Withdraw;

use App\Services\ComissionCalculator\Calculator\BaseCalculator;
use App\Services\ComissionCalculator\Currency;
use App\Services\ComissionCalculator\Data\IDataStructure;

class PrivateWithdrawCalculator extends BaseCalculator
{
    /**
     * Users weekly transaction amounts
     * @var array
     */
    private array $weeklyTransactions = [];

    /**
     * Previus week of a transaction
     * @var string
     */
    private string $previous_week = '';

    /**
     * Previous year of a transaction
     * @var int
     */
    private int $previous_year = 0;

    /**
     * Previous month of a transaction
     * @var string
     */
    private string $previous_month = '';

    /**
     * Get fee rate in percent
     * @return float|int
     */
    public function getFee(): float {
        return env('PRIVATE_WITHDRAW_COMISSION_FEE') / 100;
    }

    /**
     * Calculate commission
     * @param IDataStructure $transaction
     * @return float|int
     */
    public function calculate(IDataStructure $transaction)
    {
        $userId = $transaction->getUserId();
        $date = $transaction->getDate();
        $currencyName = $transaction->getCurrency();
        $currency = new Currency();
        $amount = $transaction->getAmount();
        $amountInEur = $currency->convertToBaseCurrency($amount, $currencyName);
        $comission = $amount * $this->getFee();
        $freeWeeklyComissionAmount = env('FREE_WEEKLY_BUSINESS_WITHDRAW_COMISSION_AMOUNT');

        // Get users weekly transactions and calculate sum of them before calculating comission
        $usersWeeklyTransactions = $this->getWeeklyTransactionsOfUser($userId, $date);
        $sumOfWeeklyTransactions = array_sum($usersWeeklyTransactions);
        if ($sumOfWeeklyTransactions < $freeWeeklyComissionAmount) {
            $comissionInEur = ($sumOfWeeklyTransactions + $amountInEur - $freeWeeklyComissionAmount) * $this->getFee();
            $comission = $currency->convertFromBaseCurrency($comissionInEur, $currencyName);
        }

        // Check if number od weekly transactions of user is greater or equal 3
        if(count($usersWeeklyTransactions) >= env('NUMBER_OF_WEEKLY_FREE_COMISSIONS')) {
            $comission = $amount * $this->getFee() ;
        }

        $this->addTransactionHistory($userId, $date, $amountInEur);
        $decimals = $this->getNumberOfDecimals($amount, $currencyName);
        $roundedComission = $this->roundUpComissionFee($comission, $decimals);
        return max($roundedComission, 0);
    }

    /**
     * Add amount to users weekly transactions
     * @param $id
     * @param $date
     * @param $amount
     * @return void
     */
    public function addTransactionHistory($id, $date, $amount):void
    {
        $k = $this->makeWeeklyTransactionKey($id, $date);
        $this->weeklyTransactions[$k][] = $amount;
    }

    /**
     * Get users weekly transactions
     * @param $id
     * @param $date
     * @return array|mixed
     */
    public function getWeeklyTransactionsOfUser($id, $date):array
    {
        $k = $this->makeWeeklyTransactionKey($id, $date);
        if(array_key_exists($k, $this->weeklyTransactions)) {
            return $this->weeklyTransactions[$k];
        }
        $this->weeklyTransactions[$k] = [];
        return $this->weeklyTransactions[$k];
    }

    /**
     * Make users weekly transaction key and set previous year, month and week
     * @param $id
     * @param $date
     * @return string
     */
    protected function makeWeeklyTransactionKey($id, $date):string {
        // First we need to get previous week, month and year
        $previous_week = $this->previous_week;
        $previous_year = $this->previous_year;
        $previous_month = $this->previous_month;

        // Then we need to set get current week, month and year and set them to previous ones
        $current_week = date('W', strtotime($date));
        $current_year = date('Y', strtotime($date));
        $current_month = date('m', strtotime($date));
        $this->previous_week = $current_week;
        $this->previous_year = $current_year;
        $this->previous_month = $current_month;

        // We need to check if the last and the current week are equal, last month is december, year difference is 1.
        // It will help correctly calculate weekly free comission
        if($current_week == $previous_week && ($current_year - $previous_year) == 1 && $previous_month == 12) {
            return $id.'_'.$id.'_'.$previous_year.'_'.$current_week;
        }
        return $id.'_'.$id.'_'.$current_year.'_'.$current_week;
    }
}
