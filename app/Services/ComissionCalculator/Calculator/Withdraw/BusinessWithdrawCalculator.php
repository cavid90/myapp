<?php

namespace App\Services\ComissionCalculator\Calculator\Withdraw;

use App\Services\ComissionCalculator\Calculator\BaseCalculator;
use App\Services\ComissionCalculator\Data\IDataStructure;

class BusinessWithdrawCalculator extends BaseCalculator
{
    /**
     * Get fee rate in percent
     * @return float|int
     */
    public function getFee() {
        return env('BUSINESS_WITHDRAW_COMISSION_FEE') / 100;
    }

    /**
     * Calculate
     * @param IDataStructure $transaction
     * @return float|int
     */
    public function calculate(IDataStructure $transaction)
    {
        $decimals = $this->getNumberOfDecimals($transaction->getAmount(), $transaction->getCurrency());
        return $this->roundUpComissionFee($transaction->getAmount() * $this->getFee(), $decimals);
    }
}
