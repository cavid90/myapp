<?php

namespace App\Services\ComissionCalculator\Calculator;

use App\Services\ComissionCalculator\Data\IDataStructure;

abstract class BaseCalculator
{
    /**
     * Return fee rate in percent
     * @return mixed
     */
    abstract public function getFee();

    /**
     * Calculate commission
     * @param IDataStructure $transaction
     * @return float
     */
    abstract public function calculate(IDataStructure $transaction);

    /**
     * Round up comission fee
     * @param float|int $fee
     * @param int $precision
     * @return float|int
     */
    public function roundUpComissionFee(float $fee, int $precision = 2) {
        $fig = pow(10, $precision);
        return ceil(bcmul($fee,$fig, 2)) / $fig;
    }

    /**
     * Get number of decimals of an amount. Example: 9.35 has 2 decimals (3 and 5), 3000 has no decimals
     * @param float|int $amount
     * @return int
     */
    public function getNumberOfDecimals($amount, $currency): int
    {
        if((bool)env('USE_DECIMALS_CONFIG') == true) {
            return config('app.decimals.'.$currency);
        }
        return strlen(substr(strrchr($amount, "."), 1));
    }
}
