<?php

namespace App\Services\ComissionCalculator\FileParser;

use App\Services\ComissionCalculator\Data\DataStructure;
use App\Services\ComissionCalculator\Data\IDataStructure;

abstract class BaseParser
{
    /**
     * Parsed data
     * @var array
     */
    protected array $data;

    /**
     * Transformed version of parsed data
     * @var array
     */
    protected array $transformedData;

    /**
     * Parse file to array
     * @param $filename
     * @return mixed
     */
    abstract function parse($filename);

    /**
     * Get data
     * @return array
     */
    public function getParsedData(): array
    {
        return $this->data;
    }

    /**
     * Transform data to the structure which we need
     * @return void
     */
    public function transformData():void {
        foreach ($this->data as $d) {
            $newData = new DataStructure($d);
            $this->transformedData[] = $newData;
        }
    }

    /**
     * Get transformed data
     * @return DataStructure[]
     */
    public function getTransformedData(): array
    {
        return $this->transformedData;
    }
}
