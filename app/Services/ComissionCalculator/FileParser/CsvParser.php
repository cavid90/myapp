<?php

namespace App\Services\ComissionCalculator\FileParser;

class CsvParser extends BaseParser
{
    /**
     * Read csv file and make array from its lines
     * @param $filename
     * @return void
     * @throws \Exception
     */
    public function parse($filename) {
        if(!is_file(app()->basePath(env('CSV_FILE_DIR').$filename))) {
            throw new \Exception('File not found');
        }
        $file = fopen(app()->basePath(env('CSV_FILE_DIR').$filename), 'r');
        while(($line = fgetcsv($file)) !== false) {
            $columns = explode(';', $line[0]);
            $this->data[] = $columns;
        }
    }
}
