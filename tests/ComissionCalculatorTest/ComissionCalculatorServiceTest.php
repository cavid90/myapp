<?php

namespace tests\ComissionCalculatorTest;

use App\Services\ComissionCalculator\ComissionCalculatorService;
use App\Services\ComissionCalculator\Data\DataStructure;
use tests\TestCase;

class ComissionCalculatorServiceTest extends TestCase
{
    protected string $fileName = 'inputs1.csv';

    /**
     * Test calculation
     * @return void
     * @throws \Exception
     */
    public function testCalculateCommission()
    {
        $commissionCalculator = new ComissionCalculatorService();
        $transactions = $this->transformTestData();
        $commissions = [];
        foreach ($transactions as $t) {
            $commission = $commissionCalculator->calculateComission($t);
            $commissions[] = $commission;
        }

        $this->assertEquals($this->calculatedCommissionData(), $commissions,  0.00001);
    }

    /**
     * Check if transformed data are equal
     * @return void
     */
    public function testCheckIfTransformedDataIsEqual()
    {
        $filename = $this->fileName;
        $array = explode('.', $filename);
        $ext = end($array);
        $parserClassName = config('app.fileParsers')[$ext];
        $parser = app()->make($parserClassName);
        $parser->parse($filename);
        $parser->transformData();
        $data = $parser->getTransformedData();

        $this->assertEquals($this->transformTestData(), $data, 'Data is equal');
    }


    /**
     * Data for test
     * @return array[]
     */
    public function getTestData() {
        return [
            ['2014-12-31',4,'private','withdraw',1200.00,'EUR'],
            ['2015-01-01',4,'private','withdraw',1000.00,'EUR'],
            ['2016-01-05',4,'private','withdraw',1000.00,'EUR'],
            ['2016-01-05',1,'private','deposit',200.00,'EUR'],
            ['2016-01-06',2,'business','withdraw',300.00,'EUR'],
            ['2016-01-06',1,'private','withdraw',30000,'JPY'],
            ['2016-01-07',1,'private','withdraw',1000.00,'EUR'],
            ['2016-01-07',1,'private','withdraw',100.00,'USD'],
            ['2016-01-10',1,'private','withdraw',100.00,'EUR'],
            ['2016-01-10',2,'business','deposit',10000.00,'EUR'],
            ['2016-01-10',3,'private','withdraw',1000.00,'EUR'],
            ['2016-02-15',1,'private','withdraw',300.00,'EUR'],
            ['2016-02-19',5,'private','withdraw',3000000,'JPY'],
        ];
    }

    /**
     * Transfrom test data to data which we need
     * @dataProvider getTestData
     * @return array
     */
    public function transformTestData() {
        $data = [];
        foreach ($this->getTestData() as $d) {
            $newData = new DataStructure($d);
            $data[] = $newData;
        }

        return $data;
    }

    /**
     * Calculated commission data for test
     * @return array
     */
    public function calculatedCommissionData() {
        return [0.6, 3.0, 0.0, 0.06, 1.5, 0, 0.7, 0.3, 0.3, 3.0, 0.0, 0, 8612.0];
    }
}
